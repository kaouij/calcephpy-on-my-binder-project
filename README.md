# Calceph's Notebook
[Calceph](https://www.imcce.fr/inpop/calceph) is a library designed to access different binary planetary ephemeris data files developped by the [IMCCE](https://www.imcce.fr/).
This GitLab project contains some examples of the use of the Python interface of Calceph with the Jupyter interface provided by MyBinder. To display the examples, click on the following badge :

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.obspm.fr%2Fkaouij%2Fcalcephpy-on-my-binder-project.git/3a4c6756a1505467c8f7647d6e7d865490bf721b?filepath=index.ipynb)

### Download and install the library 

The latest version of Calceph can be downloaded from [Calceph website](https://www.imcce.fr/inpop/calceph) 
